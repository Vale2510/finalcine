USE [Cine]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 04/12/2019 05:24:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id_usuario] [int] NOT NULL,
	[Nom_usu] [varchar](50) NULL,
	[account] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[Status_admin] [bit] NULL,
	[Foto] [varchar](500) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 04/12/2019 05:24:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[id_clientes] [int] NULL,
	[Nom_cli] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Articulo]    Script Date: 04/12/2019 05:24:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Articulo](
	[id_pro] [int] NULL,
	[Nom_pro] [varchar](50) NULL,
	[Precio] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[ActualizarArticulos]    Script Date: 04/12/2019 05:24:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ActualizarArticulos]

@Id_pro int, @Nom_pro varchar(100), @Precio float

as 

if NOT EXISTS (SELECT Id_pro FROM Articulo where id_pro=@Id_pro)
insert into Articulo (id_pro,Nom_pro,Precio) values (@Id_pro,@Nom_pro,@Precio)

else 

update  Articulo set id_pro=@Id_pro, Nom_pro=@Nom_pro, Precio=@Precio
GO
