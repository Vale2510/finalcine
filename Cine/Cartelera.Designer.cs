﻿namespace Cine
{
    partial class Cartelera
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPropietiesZasham = new System.Windows.Forms.Button();
            this.btnPropietiesAdosMetros = new System.Windows.Forms.Button();
            this.btnPropietiesCeme = new System.Windows.Forms.Button();
            this.btnPropietiesDumboo = new System.Windows.Forms.Button();
            this.btnRetourn = new System.Windows.Forms.Button();
            this.lblCartelera = new System.Windows.Forms.Label();
            this.pictureZasham = new System.Windows.Forms.PictureBox();
            this.pictureCementerio = new System.Windows.Forms.PictureBox();
            this.pictureAdosMetros = new System.Windows.Forms.PictureBox();
            this.pictureDumbo = new System.Windows.Forms.PictureBox();
            this.btnVenta = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureZasham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCementerio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureAdosMetros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureDumbo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPropietiesZasham
            // 
            this.btnPropietiesZasham.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnPropietiesZasham.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPropietiesZasham.Location = new System.Drawing.Point(632, 294);
            this.btnPropietiesZasham.Name = "btnPropietiesZasham";
            this.btnPropietiesZasham.Size = new System.Drawing.Size(138, 27);
            this.btnPropietiesZasham.TabIndex = 19;
            this.btnPropietiesZasham.Text = "Ver Trailer";
            this.btnPropietiesZasham.UseVisualStyleBackColor = false;
            this.btnPropietiesZasham.Click += new System.EventHandler(this.btnPropietiesZasham_Click);
            // 
            // btnPropietiesAdosMetros
            // 
            this.btnPropietiesAdosMetros.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnPropietiesAdosMetros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPropietiesAdosMetros.Location = new System.Drawing.Point(244, 294);
            this.btnPropietiesAdosMetros.Name = "btnPropietiesAdosMetros";
            this.btnPropietiesAdosMetros.Size = new System.Drawing.Size(127, 27);
            this.btnPropietiesAdosMetros.TabIndex = 17;
            this.btnPropietiesAdosMetros.Text = "Ver Trailer ";
            this.btnPropietiesAdosMetros.UseVisualStyleBackColor = false;
            this.btnPropietiesAdosMetros.Click += new System.EventHandler(this.btnPropietiesAdosMetros_Click);
            // 
            // btnPropietiesCeme
            // 
            this.btnPropietiesCeme.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnPropietiesCeme.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPropietiesCeme.Location = new System.Drawing.Point(445, 294);
            this.btnPropietiesCeme.Name = "btnPropietiesCeme";
            this.btnPropietiesCeme.Size = new System.Drawing.Size(138, 27);
            this.btnPropietiesCeme.TabIndex = 16;
            this.btnPropietiesCeme.Text = "Ver Trailer";
            this.btnPropietiesCeme.UseVisualStyleBackColor = false;
            this.btnPropietiesCeme.Click += new System.EventHandler(this.btnPropietiesCeme_Click);
            // 
            // btnPropietiesDumboo
            // 
            this.btnPropietiesDumboo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnPropietiesDumboo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPropietiesDumboo.Location = new System.Drawing.Point(32, 294);
            this.btnPropietiesDumboo.Name = "btnPropietiesDumboo";
            this.btnPropietiesDumboo.Size = new System.Drawing.Size(131, 27);
            this.btnPropietiesDumboo.TabIndex = 15;
            this.btnPropietiesDumboo.Text = "Ver Trailer";
            this.btnPropietiesDumboo.UseVisualStyleBackColor = false;
            this.btnPropietiesDumboo.Click += new System.EventHandler(this.btnPropietiesDumboo_Click);
            // 
            // btnRetourn
            // 
            this.btnRetourn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetourn.Location = new System.Drawing.Point(542, 357);
            this.btnRetourn.Name = "btnRetourn";
            this.btnRetourn.Size = new System.Drawing.Size(192, 42);
            this.btnRetourn.TabIndex = 14;
            this.btnRetourn.Text = "Regresar al Menú";
            this.btnRetourn.UseVisualStyleBackColor = true;
            this.btnRetourn.Click += new System.EventHandler(this.btnRetourn_Click);
            // 
            // lblCartelera
            // 
            this.lblCartelera.AutoSize = true;
            this.lblCartelera.BackColor = System.Drawing.Color.White;
            this.lblCartelera.Font = new System.Drawing.Font("Oswald", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCartelera.Location = new System.Drawing.Point(229, 14);
            this.lblCartelera.Name = "lblCartelera";
            this.lblCartelera.Size = new System.Drawing.Size(156, 40);
            this.lblCartelera.TabIndex = 10;
            this.lblCartelera.Text = "En Cartelera";
            // 
            // pictureZasham
            // 
            this.pictureZasham.BackgroundImage = global::Cine.Properties.Resources._3361614_jpg_c_215_290_x_f_jpg_q_x_xxyxx;
            this.pictureZasham.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureZasham.Location = new System.Drawing.Point(622, 104);
            this.pictureZasham.Name = "pictureZasham";
            this.pictureZasham.Size = new System.Drawing.Size(158, 229);
            this.pictureZasham.TabIndex = 18;
            this.pictureZasham.TabStop = false;
            // 
            // pictureCementerio
            // 
            this.pictureCementerio.BackgroundImage = global::Cine.Properties.Resources.CementerioMaldito;
            this.pictureCementerio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureCementerio.Location = new System.Drawing.Point(434, 104);
            this.pictureCementerio.Name = "pictureCementerio";
            this.pictureCementerio.Size = new System.Drawing.Size(158, 229);
            this.pictureCementerio.TabIndex = 13;
            this.pictureCementerio.TabStop = false;
            // 
            // pictureAdosMetros
            // 
            this.pictureAdosMetros.BackgroundImage = global::Cine.Properties.Resources.A_dos_Metros_de_Ti;
            this.pictureAdosMetros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureAdosMetros.Location = new System.Drawing.Point(227, 104);
            this.pictureAdosMetros.Name = "pictureAdosMetros";
            this.pictureAdosMetros.Size = new System.Drawing.Size(158, 229);
            this.pictureAdosMetros.TabIndex = 12;
            this.pictureAdosMetros.TabStop = false;
            // 
            // pictureDumbo
            // 
            this.pictureDumbo.BackgroundImage = global::Cine.Properties.Resources._2486964_jpg_c_215_290_x_f_jpg_q_x_xxyxx;
            this.pictureDumbo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureDumbo.Location = new System.Drawing.Point(21, 104);
            this.pictureDumbo.Name = "pictureDumbo";
            this.pictureDumbo.Size = new System.Drawing.Size(158, 229);
            this.pictureDumbo.TabIndex = 11;
            this.pictureDumbo.TabStop = false;
            // 
            // btnVenta
            // 
            this.btnVenta.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVenta.Location = new System.Drawing.Point(32, 366);
            this.btnVenta.Name = "btnVenta";
            this.btnVenta.Size = new System.Drawing.Size(82, 42);
            this.btnVenta.TabIndex = 20;
            this.btnVenta.Text = "Ir a venta";
            this.btnVenta.UseVisualStyleBackColor = true;
            this.btnVenta.Click += new System.EventHandler(this.btnVenta_Click);
            // 
            // Cartelera
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Cine.Properties.Resources.cinemaparaiso_4;
            this.ClientSize = new System.Drawing.Size(790, 464);
            this.Controls.Add(this.btnVenta);
            this.Controls.Add(this.btnPropietiesZasham);
            this.Controls.Add(this.pictureZasham);
            this.Controls.Add(this.btnPropietiesAdosMetros);
            this.Controls.Add(this.btnPropietiesCeme);
            this.Controls.Add(this.btnPropietiesDumboo);
            this.Controls.Add(this.btnRetourn);
            this.Controls.Add(this.pictureCementerio);
            this.Controls.Add(this.pictureAdosMetros);
            this.Controls.Add(this.pictureDumbo);
            this.Controls.Add(this.lblCartelera);
            this.Name = "Cartelera";
            this.Text = "Cartelera";
            this.Load += new System.EventHandler(this.Cartelera_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureZasham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCementerio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureAdosMetros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureDumbo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPropietiesZasham;
        private System.Windows.Forms.PictureBox pictureZasham;
        private System.Windows.Forms.Button btnPropietiesAdosMetros;
        private System.Windows.Forms.Button btnPropietiesCeme;
        private System.Windows.Forms.Button btnPropietiesDumboo;
        private System.Windows.Forms.Button btnRetourn;
        private System.Windows.Forms.PictureBox pictureCementerio;
        private System.Windows.Forms.PictureBox pictureAdosMetros;
        private System.Windows.Forms.PictureBox pictureDumbo;
        private System.Windows.Forms.Label lblCartelera;
        private System.Windows.Forms.Button btnVenta;
    }
}