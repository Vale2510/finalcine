﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;


namespace Cine
{
    public partial class Cartelera : Form
    {
        public Cartelera()
        {
            InitializeComponent();
        }
        public static String Codigo = "";

        private void btnRetourn_Click(object sender, EventArgs e)
        {

            VentanaUser miForma = new VentanaUser();

            miForma.Show();
        }

        private void btnPropietiesDumboo_Click(object sender, EventArgs e)
        {

            Trailer miForma = new Trailer();

            miForma.Show();

            this.Hide();

        }

        private void Cartelera_Load(object sender, EventArgs e)
        {
           

           

        }

        private void btnPropietiesAdosMetros_Click(object sender, EventArgs e)
        {
            TrailerAdosmetro miForma = new TrailerAdosmetro();

            miForma.Show();

            this.Hide();
        }

        private void btnPropietiesCeme_Click(object sender, EventArgs e)
        {
            TrailerCmenterio miForma = new TrailerCmenterio();

            miForma.Show();

            this.Hide();
        }

        private void btnPropietiesZasham_Click(object sender, EventArgs e)
        {

            TrailerZasham miForma = new TrailerZasham();

            miForma.Show();

            this.Hide();

        }

        private void btnVenta_Click(object sender, EventArgs e)
        {
            Boleto miForma = new Boleto();

            miForma.Show();

            this.Hide();
        }
    }
}
