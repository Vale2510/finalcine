﻿namespace Cine
{
    partial class Informacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPelicula = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblDuracion = new System.Windows.Forms.Label();
            this.lblaño = new System.Windows.Forms.Label();
            this.lblAutor = new System.Windows.Forms.Label();
            this.lblEspecificación = new System.Windows.Forms.Label();
            this.lblNomPeli = new System.Windows.Forms.Label();
            this.lblNomAu = new System.Windows.Forms.Label();
            this.lblAno = new System.Windows.Forms.Label();
            this.lblDurac = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPelicula
            // 
            this.lblPelicula.AutoSize = true;
            this.lblPelicula.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblPelicula.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPelicula.Location = new System.Drawing.Point(268, 160);
            this.lblPelicula.Name = "lblPelicula";
            this.lblPelicula.Size = new System.Drawing.Size(62, 18);
            this.lblPelicula.TabIndex = 34;
            this.lblPelicula.Text = "Pelicula:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(55, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(172, 178);
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            // 
            // lblDuracion
            // 
            this.lblDuracion.AutoSize = true;
            this.lblDuracion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDuracion.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuracion.Location = new System.Drawing.Point(268, 326);
            this.lblDuracion.Name = "lblDuracion";
            this.lblDuracion.Size = new System.Drawing.Size(70, 18);
            this.lblDuracion.TabIndex = 27;
            this.lblDuracion.Text = "Duración:";
            // 
            // lblaño
            // 
            this.lblaño.AutoSize = true;
            this.lblaño.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblaño.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblaño.Location = new System.Drawing.Point(268, 273);
            this.lblaño.Name = "lblaño";
            this.lblaño.Size = new System.Drawing.Size(39, 18);
            this.lblaño.TabIndex = 26;
            this.lblaño.Text = "Año:";
            // 
            // lblAutor
            // 
            this.lblAutor.AutoSize = true;
            this.lblAutor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAutor.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutor.Location = new System.Drawing.Point(268, 213);
            this.lblAutor.Name = "lblAutor";
            this.lblAutor.Size = new System.Drawing.Size(53, 18);
            this.lblAutor.TabIndex = 25;
            this.lblAutor.Text = "Autor: ";
            // 
            // lblEspecificación
            // 
            this.lblEspecificación.AutoSize = true;
            this.lblEspecificación.BackColor = System.Drawing.Color.White;
            this.lblEspecificación.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEspecificación.Location = new System.Drawing.Point(309, 49);
            this.lblEspecificación.Name = "lblEspecificación";
            this.lblEspecificación.Size = new System.Drawing.Size(314, 29);
            this.lblEspecificación.TabIndex = 24;
            this.lblEspecificación.Text = "Especificación de Pelicula";
            // 
            // lblNomPeli
            // 
            this.lblNomPeli.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNomPeli.Location = new System.Drawing.Point(405, 143);
            this.lblNomPeli.Name = "lblNomPeli";
            this.lblNomPeli.Size = new System.Drawing.Size(159, 23);
            this.lblNomPeli.TabIndex = 35;
            this.lblNomPeli.Click += new System.EventHandler(this.lblNomUs_Click);
            // 
            // lblNomAu
            // 
            this.lblNomAu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNomAu.Location = new System.Drawing.Point(405, 198);
            this.lblNomAu.Name = "lblNomAu";
            this.lblNomAu.Size = new System.Drawing.Size(159, 23);
            this.lblNomAu.TabIndex = 36;
            this.lblNomAu.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblAno
            // 
            this.lblAno.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAno.Location = new System.Drawing.Point(405, 250);
            this.lblAno.Name = "lblAno";
            this.lblAno.Size = new System.Drawing.Size(159, 23);
            this.lblAno.TabIndex = 37;
            // 
            // lblDurac
            // 
            this.lblDurac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDurac.Location = new System.Drawing.Point(405, 321);
            this.lblDurac.Name = "lblDurac";
            this.lblDurac.Size = new System.Drawing.Size(159, 23);
            this.lblDurac.TabIndex = 38;
            // 
            // Informacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.BackgroundImage = global::Cine.Properties.Resources.cine4;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(704, 413);
            this.Controls.Add(this.lblDurac);
            this.Controls.Add(this.lblAno);
            this.Controls.Add(this.lblNomAu);
            this.Controls.Add(this.lblNomPeli);
            this.Controls.Add(this.lblPelicula);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblDuracion);
            this.Controls.Add(this.lblaño);
            this.Controls.Add(this.lblAutor);
            this.Controls.Add(this.lblEspecificación);
            this.Name = "Informacion";
            this.Text = "Informacion";
            this.Load += new System.EventHandler(this.Informacion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblPelicula;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDuracion;
        private System.Windows.Forms.Label lblaño;
        private System.Windows.Forms.Label lblAutor;
        private System.Windows.Forms.Label lblEspecificación;
        private System.Windows.Forms.Label lblNomPeli;
        private System.Windows.Forms.Label lblNomAu;
        private System.Windows.Forms.Label lblAno;
        private System.Windows.Forms.Label lblDurac;
    }
}